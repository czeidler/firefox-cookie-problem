
var send = function(callback) {
	var request = new XMLHttpRequest();
	request.open("POST", "https://fejoa.org/fejoa.portal");
	request.onreadystatechange = function() {
		if (request.readyState == XMLHttpRequest.DONE) {
			callback();
		}
	}
	request.send();
}

// Send two requests
// - the first reply has a Set-Cookie header
// - the second request doesn't use this cookie
// - the second reply has another Set-Cookie header
send(function() {
	send(function() {
	});
});

